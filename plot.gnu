reset
set ylabel title
set datafile separator ','
set style fill solid
set rmargin 5
set tmargin 2
set lmargin 5
set xtics rotate offset 0.6,0
unset ytics
set y2tics center rotate by 90
set boxwidth 2
stats filename using 2 nooutput
set yrange [0:((STATS_max)/600)*700]
set terminal png size STATS_records*20+200, 700 font "Helvetica,12" background rgb "#C0C0C0"
#plot "< sort -rnk 2 -t , ".filename using 2:xtic(1) with histogram notitle linecolor rgb "#F0C000",\
#     '' using 0:2:2:x2tic(2) with labels center rotate offset 0.6,1.4 notitle
plot "< sort -rnk 2 -t , ".filename using 2:xtic(1) with histogram notitle linecolor rgb "#C08000",\
     '' using 0:2:2:x2tic(2) with labels center rotate offset 0.6,1.4 notitle
