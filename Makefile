.PHONY: clean upload all data

PYTHON = venv/bin/python

TESTS = test_merge_outputs test_kw_keys_create test_kw_keys_get test_map_create_vs_mutate
TESTS_DATA = $(TESTS:=_data)
TESTS_IMGS = $(TESTS:=.png)

HYSRC = *.hy

all:	$(TESTS_DATA) $(TESTS_IMGS)

data:	$(TESTS_DATA)

%_data:	%.py venv $(HYSRC)
	$(PYTHON) $< > $@

%.png:	%_data plot.gnu
	gnuplot -e "filename='$<'" -e "title=\"`head -1 test_kw_keys_create_data|cut -c3-`\"" plot.gnu > $@
	mogrify -rotate 90 $@

venv: venv/bin/activate

venv/bin/activate: requirements.txt
	test -d venv || virtualenv -p python3 venv
	venv/bin/pip install -Ur requirements.txt
	touch venv/bin/activate

clean:
	rm -rf venv __pycache__ $(TESTS_IMGS) $(TESTS_DATA)
