from itertools import chain
from functools import reduce
import timeit
import hy
from merge_outputs_hy import *

def append_to_msgs(m, msg):
  if "msgs" in m:
    m["msgs"].append(msg)
  else:
    m["msgs"] = [msg]

def merge_outputs_py(outputs):
  """Python version of the original "merge-outputs" function -- exactly the same as in Hy, but with string keys"""
  def merge(m, e):
    [k, v] = e
    if k == 'msg':
      append_to_msgs(m, v)
    else:
      m[k] = v
    return m
  return reduce(merge, chain(*map(lambda d: d.items() if d else [], outputs)), {})

def merge_outputs_py2(outputs):
  """Slightly improved Python version: using chain_from_iterable"""
  def merge(m, e):
    [k, v] = e
    if k == 'msg':
      append_to_msgs(m, v)
    else:
      m[k] = v
    return m
  return reduce(merge, chain.from_iterable([(dct.items() if dct else []) for dct in outputs]), {})

merge_outputs_test_data = [
  {"speed": 5, "dir": 30},
  {"msg": "hello"},
  {"battery-voltage": 3.8},
  None,
  {"speed": 0, "dir": 0, "msg": "emergency stop"}
]
def test_merge_outputs_py():
  merge_outputs_py(merge_outputs_test_data)
def test_merge_outputs_py2():
  merge_outputs_py2(merge_outputs_test_data)

if __name__ == '__main__':
  import timeit
  print("# merge-ouputs improvements")
  for fn in [("test_merge_outputs_py", "Original (Python)"),
             ("test_merge_outputs_py2", "Improved (Python)"),
             ("test_merge_outputs_hy", "Original (Hy)"),
             ("test_merge_outputs_hy_2", "Improved (Hy)"),
             ("test_merge_outputs_ext_hy", "External merge (Hy)"),
             ("test_merge_outputs_hy_no_kw", "No keywords (Hy)"),
             ("test_merge_outputs_hy_no_kw_imperative", "Imperative (Hy)")]:
    print("{}, {}".format(fn[1], round(min(timeit.repeat("__main__.{}()".format(fn[0]), setup="import __main__", number=100000)) * 1000)))

