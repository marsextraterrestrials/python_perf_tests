import timeit
import hy
from hy import HyKeyword

def test_get_kw_keys():
    return get_kw_keys_testdata.get(HyKeyword("abc")), get_kw_keys_testdata.get(HyKeyword("def")), get_kw_keys_testdata.get(HyKeyword("xxx")), get_kw_keys_testdata.get(HyKeyword("yyy"))

get_str_keys_testdata = { "abc": 1, "def": 2, "ghi": 3 }
def test_get_str_keys():
    return get_str_keys_testdata.get("abc"), get_kw_keys_testdata.get("def"), get_kw_keys_testdata.get("xxx"), get_kw_keys_testdata.get("yyy")

get_kw_keys_testdata = { HyKeyword("abc"): 1, HyKeyword("def"): 2, HyKeyword("ghi"): 3 }
abc_kw, def_kw, ghi_kw, xxx_kw, yyy_kw = HyKeyword("abc"), HyKeyword("def"), HyKeyword("ghi"), HyKeyword("xxx"), HyKeyword("yyy")
def test_get_kw_keys_consts():
    return get_kw_keys_testdata.get(abc_kw), get_kw_keys_testdata.get(def_kw), get_kw_keys_testdata.get(xxx_kw), get_kw_keys_testdata.get(yyy_kw)


if __name__ == '__main__':
    import timeit
    print("# Access by keyword or string keys")
    for fn in [("test_get_kw_keys", "Keyword keys"),
               ("test_get_kw_keys_consts", "Predefined keyword keys"),
               ("test_get_str_keys", "String keys")]:
        print("{}, {}".format(fn[1], round(min(timeit.repeat("__main__.{}()".format(fn[0]), setup="import __main__", number=100000)) * 1000)))
