(defn append-to-msgs [m msg]
  (if (in :msgs m)
      (.append (:msgs m) msg)
      (assoc m :msgs [msg])))

(defn merge-outputs-hy [outputs]
  """Original version"""
  (defn merge [m e]
    (setv [k v] e)
    (if (= k :msg)
        (append-to-msgs m v)
        (assoc m k v))
    m)
  (reduce merge (chain #*(map (fn [d] (if d (.items d) [])) outputs)) {} ))

(defn merge-outputs-hy-2 [outputs]
  """Improved version using chain.from_iterable"""
  (defn merge [m e]
    (setv [k v] e)
    (if (= k :msg)
        (append-to-msgs m v)
        (assoc m k v))
    m)
  (reduce merge (chain.from_iterable (lfor dct outputs (if dct (.items dct) []))) {}))


(defn merge-ext [m e]
  (setv [k v] e)
  (if (= k :msg)
      (append-to-msgs m v)
      (assoc m k v))
  m)
(defn merge-outputs-ext-hy [outputs]
  """Same as merge-outputs-hy but with the 'merge' function extracted to top level"""
  (reduce merge-ext (chain #*(map (fn [d] (if d (.items d) [])) outputs)) {} ))


(defn append-to-msgs-no-kw [m msg]
  (if (in "msgs" m)
      (.append (get m "msgs") msg)
      (assoc m "msgs" [msg])))

(defn merge-outputs-hy-no-kw [outputs]
  """Replacing keyword keys with strings"""
  (defn merge [m e]
    (setv [k v] e)
    (if (= k "msg")
        (append-to-msgs-no-kw m v)
        (assoc m k v))
    m)
  (reduce merge (chain #*(map (fn [d] (if d (.items d) [])) outputs)) {} ))

(defn merge-outputs-hy-no-kw-imperative [outputs]
  """Imperative version with a for loop - also with string keys"""
  (setv ret {})
  (for [output outputs]
    (if output
        (do (setv msg (.get output "msg"))
            (if msg
                (append-to-msgs-no-kw ret msg))
            (.update ret output))))
  (if (in "msg" ret)
      (del (get ret "msg")))
  ret)


(setv merge-outputs-hy-test-data [{:speed 5  :dir 30}
                                  {:msg "hello"}
                                  {:battery-voltage 3.8}
                                  None
                                  {:speed 0  :dir 0  :msg "emergency stop"}])
(defn test-merge-outputs-hy []
  (merge-outputs-hy merge-outputs-hy-test-data))

(defn test-merge-outputs-hy-2 []
  (merge-outputs-hy-2 merge-outputs-hy-test-data))

(defn test-merge-outputs-ext-hy []
  (merge-outputs-ext-hy merge-outputs-hy-test-data))


(setv merge-outputs-hy-no-kw-test-data [{"speed" 5  "dir" 30}
                                        {"msg" "hello"}
                                        {"battery-voltage" 3.8}
                                        None
                                        {"speed" 0  "dir" 0  "msg" "emergency stop"}])
(defn test-merge-outputs-hy-no-kw []
  (merge-outputs-hy-no-kw merge-outputs-hy-no-kw-test-data))


(defn test-merge-outputs-hy-no-kw-imperative []
  (merge-outputs-hy-no-kw-imperative merge-outputs-hy-no-kw-test-data))


