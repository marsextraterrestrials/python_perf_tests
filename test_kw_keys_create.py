import timeit
import hy
from hy import HyKeyword

def test_set_kw_keys():
    return { HyKeyword("abc"): 1, HyKeyword("def"): 2, HyKeyword("ghi"): 3 }

def test_set_str_keys():
    return { "abc": 1, "def": 2, "ghi": 3 }

abc_kw, def_kw, ghi_kw = HyKeyword("abc"), HyKeyword("def"), HyKeyword("ghi")
def test_set_kw_keys_consts():
    return { abc_kw: 1, def_kw: 2, ghi_kw: 3 }


if __name__ == '__main__':
    import timeit
    print("# Create maps with keyword or string keys")
    for fn in [("test_set_kw_keys", "Keyword keys"),
               ("test_set_str_keys", "String keys"),
               ("test_set_kw_keys_consts", "Predefined keyword keys")]:
        print("{}, {}".format(fn[1], round(min(timeit.repeat("__main__.{}()".format(fn[0]), setup="import __main__", number=100000)) * 1000)))
