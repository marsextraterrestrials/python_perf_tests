from itertools import chain
import timeit


def update_as_new(old, new):
    """Merge contents of new output into old, removing keys with None values in new, and also 'msg' keys unless there is a new 'msg' in new"""
    def filter_old(it):
        [k, v] = it
        return not (k in new and new[k] is None or k == 'msg')
    return { k: v for [k, v] in chain(filter(filter_old, old.items()),
                                      filter(lambda it: not it[1] is None, new.items())) } if new else {
                                          k: v for [k, v] in filter(lambda it: it[0] != 'msg', old.items())
                                      } if 'msg' in old else old

def update_in_place(old, new):
    if "msg" in old:
        del(old["msg"])
    if new:
        for k, v in new.items():
            if v is None and k in old:
                del(old[k])
            else:
                old[k] = v

def test_update_as_new():
    old1 = {"a": 1, "b": 2, "c": 3, "msg": "hello"}
    new1 = {"a": None, "b": 22}
    update_as_new(old1, new1)
    old2 = {"a": 1, "b": 2, "c": 3, "msg": "hello"}
    new2 = None
    update_as_new(old2, new2)

def test_update_in_place():
    old1 = {"a": 1, "b": 2, "c": 3, "msg": "hello"}
    new1 = {"a": None, "b": 22}
    update_in_place(old1, new1)
    old2 = {"a": 1, "b": 2, "c": 3, "msg": "hello"}
    new2 = None
    update_in_place(old2, new2)
    
if __name__ == '__main__':
    import timeit
    print("# Create new map vs. mutating in place")    
    for fn in [("test_update_as_new", "Create new map"),
               ("test_update_in_place", "Mutate in place")]:
        print("{}, {}".format(fn[1], round(min(timeit.repeat("__main__.{}()".format(fn[0]), setup="import __main__", number=100000)) * 1000)))
